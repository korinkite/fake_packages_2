#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""The main module which copies every important file in this package."""

import os
import shutil
import sys


def build(source_path, install_path):
    for folder in ("python", ):
        destination = os.path.join(install_path, folder)

        if os.path.isdir(destination):
            shutil.rmtree(destination)

        source = os.path.join(source_path, folder)
        shutil.copytree(source, destination)


if __name__ == "__main__":
    build(
        source_path=os.environ["REZ_BUILD_SOURCE_PATH"],
        install_path=os.environ["REZ_BUILD_INSTALL_PATH"],
    )
