#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""A fake "utility" Rez package that other "client" Rez packages import and use."""

name = "bar"

version = "0.0.1"

build_command = "python {root}/rezbuild.py {install}"


def commands():
    """Add the bar package's Python files."""
    import os

    env.PYTHONPATH.append(os.path.join('{root}', 'python'))
